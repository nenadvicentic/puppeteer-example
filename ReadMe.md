# Puppeteer API automation of Chromium browser #

Google's [Pupeeteer API](https://github.com/GoogleChrome/puppeteer "Headless Chrome Node API") is used to control Chromim browser, for purposes of end to end testing.

## Installation requirements ##
Install NodeJS 8.9+
Global NPM installation of Typescript: `npm i -g typescript`
Execute `npm install` within the project folder.

## Transpile Typescript to JavaScript ##
Execute: `tsc` command from the console. This will transpile TypeScript files into Javascript files in `/dist` folder. TypeScript compiler is configured with `tsconfig.json` file in the root of the project.

To run TypeScript compiler in the mode where it monitors file changes, start it in a separate console window with: `tsc --watch`.

## Run program ##
Execute `npm start` command. Command is configured in NPM's `package.json` file in the root of the folder.


