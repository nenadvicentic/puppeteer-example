import puppeteer = require("puppeteer");

async function takeScreenshot() {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto("https://example.com");
    await page.screenshot({path: "example.png"});

    await browser.close();
  }

  async function searchForPuppeteer() {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto("https://www.google.com/ncr");
    await page.waitForSelector("input[name=q]");
    await page.focus("input[name=q]");
    await page.keyboard.type("puppeteer");
    const btnK = await page.$("input[name=btnK]");
    if (btnK != null) {
        btnK.click();
    }
    await page.waitForNavigation({waitUntil: "domcontentloaded"});
    await page.waitFor(1000);
    await browser.close();
  }

  async function openWebSnifferNet() {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto("https://websniffer.cc/");

    await page.waitForSelector("select#ua-list");

    const optionValue = await page.evaluate(() => {
        const allOptions = document.querySelectorAll("select#ua-list > option");
        let innerResult;
        allOptions.forEach((optionElement) => {
            if (optionElement.textContent === "your user agent") {
                innerResult = optionElement.getAttribute("value");
                return;
            }
        });
        return innerResult;
    });
    if (optionValue == null) {
        return;
    }
    await page.select("select#ua-list", optionValue);
    const urlInputElement = await page.$("input#url");
    if (urlInputElement == null) {
        return;
    }
    await urlInputElement.focus();
    await page.keyboard.type("www.google.com/ncr");
    await page.click("input[name=submit]");
}

 //searchForPuppeteer();
openWebSnifferNet();
